# Program configurations

Here you can find the dotfiles for my bspwm rice and other programs I use.

**NOTE:** For Arch I use the [`yay`](https://aur.archlinux.org/packages/yay) AUR Helper. Feel free to use a different one or download the AUR packages yourself. I will use packages from both the Arch packages and the AUR within the same command though.

**NOTE:** This only includes the graphical interface, no other stuff such as a sound server. Please install that using your distribution's instructions if it has not been installed already.

**NOTE:** This will most likely work on other distributions as well, but you will have to research the packages and other things on your own. I will only provide instructions for Arch and Gentoo as this is what I use.

**NOTE:** There are dotfiles provided for `kitty` and there's a whole guide about `st` as well down below. Feel free to use either, or whatever you prefer instead.

## bspwm
![Screenshot](assets/screenshot.png)

To install `bspwm` and all necessary programs that are set up within the configs:

_**Debian/Ubuntu**_
``` bash
apt install bspwm sxhkd picom feh rofi kitty
```

_**Arch**_
``` bash
yay -S bspwm sxhkd picom feh rofi kitty
```

_**Gentoo**_
``` bash
emerge -av x11-wm/bspwm x11-misc/sxhkd x11-misc/picom media-gfx/feh x11-misc/rofi x11-terms/kitty 
```

The `.config/bspwm/bspwmrc` file is set up to set the background using `feh`. However, if you want to be able to dynamically change the background rather than having a set file within the config, consider using `nitrogen` instead.

Don't forget to move the `Backgrounds` folder into your home directory as well so you have a wallpaper!

The GTK theme I use is Abrus-Dark and can be installed using:

_**Debian/Ubuntu**_
``` bash
git clone https://github.com/vinceliuice/Abrus-gtk-theme.git
apt install gtk2-engines-murrine gtk2-engines-pixbuf
cd Abrus-gtk-theme
./Install
```

_**Arch**_
``` bash
yay -S abrus-gtk-theme-git
```

_**Gentoo**_
``` bash
git clone https://github.com/vinceliuice/Abrus-gtk-theme.git
emerge -av x11-themes/gtk-engines x11-themes/gtk-engines-murrine
cd Abrus-gtk-theme
./Install
```


To set it, you can either use `lxappearance`, or manually set it inside of `~/.config/gtk-3.0/settings.ini`.

For more information on configuring `bspwm`, see the [ArchWiki](https://wiki.archlinux.org/title/Bspwm) or [GentooWiki](https://wiki.gentoo.org/wiki/Bspwm) entry.

******
## Other stuff
**NOTE:** If not mentioned otherwise, just move the respective folder into your `~/.config` folder.

******
### st
Here's a little guide on how to set up `st`. It is recommended to do so instead of using `kitty` if you're on a low-spec device.

If on Arch, install `st` without AUR Helper for configuration purposes.

_**Debian/Ubuntu**_
``` bash
apt install st
```

_**Arch**_
``` bash
git clone https://aur.archlinux.org/st.git 
cd st
makepkg -si
```

_**Gentoo**_
``` bash
emerge -av x11-terms/st 
```

**NOTE:** You don't have to install `st` through your package manager, you can also download the source code and configure and install that way, as you usually would with suckless tools. But a simple unchanged configuration works fine using package managers, and you can also configure it inside of the [Arch](https://wiki.archlinux.org/title/St) and [Gentoo](https://wiki.gentoo.org/wiki/St) package managers respectively.

******

### zsh

My zsh configuration uses the [`powerlevel10k`](https://github.com/romkatv/powerlevel10k) theme along with the [`zsh-autosuggestions`](https://github.com/zsh-users/zsh-autosuggestions) and [`zsh-syntax-highlighting`](https://github.com/zsh-users/zsh-syntax-highlighting) plugins.

This install will work without `ohmyzsh` or other plugin managers, though you might use them as well if you like to do so.

**NOTE:** This setup uses the MesloLGS NerdFont to set up `powerlevel10k`. You can find the original font original fonts [here](https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k), but they are also included in this repository.


First of all, move the `zsh` folder and `.zshrc` into `~/`. Then run:

_**Debian/Ubuntu**_
``` bash
sudo apt install zsh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions /usr/share/zsh/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git /usr/share/zsh/plugins/zsh-syntax-highlighting
mv ~/zsh/powerline-fonts/* /usr/share/fonts/
rm -rf ~/zsh/powerline-fonts/
```

_**Arch**_
``` bash
yay -S zsh ttf-meslo-nerd-font-powerlevel10k zsh-theme-powerlevel10k-git zsh-syntax-highlighting-git zsh-autosuggestions-git  
```

_**Gentoo**_
``` bash
emerge -av app-shells/zsh 
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions /usr/share/zsh/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git /usr/share/zsh/plugins/zsh-syntax-highlighting
mv ~/zsh/powerline-fonts/* /usr/share/fonts/
rm -rf ~/zsh/powerline-fonts/
```

You should be able to choose the MesloLGS fonts within your terminal if you use anything other than `st`. For `st`, you will need to add the right font to your `config.h` before recompiling and reinstalling. 

_**Arch**_
``` bash
cd (AUR-Folder-Dir)/st
sed 's/Liberation Mono/MesloLGS NF/' -i src/st-0.8.5/config.h
makepkg -sif
```

_**Gentoo**_
``` bash
#Add savedconfig USE-Flag if you haven't already
cp etc/portage/package.use/10sucklesstools /etc/portage/package.use
emerge -av x11-terms/st
#Set font
sed 's/Liberation Mono/MesloLGS NF/' -i /etc/portage/savedconfig/x11-terms/st-0.8.4
emerge -av x11-terms/st
```

Once everything is set up, and you haven't configured `powerlevel10k` already, run:

```bash
zsh
p10k configure
```


If you want zsh to be your standard shell, run:
```bash
chsh -s $(which zsh)
```
Afterwards, start a new terminal session.


Feel free to check out the aliases defined in `~/zsh/aliasrc`!

******

### Ranger
The file inside of `.config/ranger` changes some of the default file associations. See [this article](https://codeyarns.com/tech/2015-03-12-how-to-set-file-association-in-ranger.html) for more information.

******

### Vim/Neovim
The Vim and Neovim configs are very similar and rudimentary, since they only enable syntax highlighting and hybrid line numbers. For Vim copy `.vimrc` into `~/`.
