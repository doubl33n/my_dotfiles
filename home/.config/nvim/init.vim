syntax on
set number relativenumber
set expandtab
set tabstop=4
set shiftwidth=4

nmap <C-L><C-L> :set invrelativenumber<CR>
