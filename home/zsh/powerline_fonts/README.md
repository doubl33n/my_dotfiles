# Powerline Fonts

These fonts are taken from the official [`powerline10k`](https://github.com/romkatv/powerlevel10k#fonts) repo.
