# Gentoo Files

Configuration files for portage and the Linux kernel.

## Portage Files
Before placing the `make.conf` file into your `/etc/portage` directory, please make sure to change the following variables to reflect your desired configuration:
```
MAKEOPTS
ACCEPT_LICENSE
USE
VIDEO_CARDS
```

Consider changing `march=native` to your respective processor architecture as per [this article](https://wiki.gentoo.org/wiki/Safe_CFLAGS) if you intend to compile packages for a different architecture, or emerge your updates with `distcc`.

If you want your OS and programs to be in a different language other than English, consider adding those lines to the `make.conf` file (using a French system as an example):
```
LINGUAS="fr en"
L10N="fr"
```
For using other languages you might want to look [here](https://wiki.gentoo.org/wiki/Localization/Guide) and [here](https://wiki.gentoo.org/wiki/UTF-8) for more information on configuring your system.

Also make sure to update your mirrors using `mirrorselect` as described [here](https://wiki.gentoo.org/wiki/Mirrorselect).

And lastly, set your `package.use`, `package.license` and `package.accept_keywords` according to your needs and portage suggestions.

## Kernel .config
This kernel was built specifically to suit the needs of my configuration. It is using options for an AMD CPU and GPU, so please change for your needs. It also includes exclusive OpenRC support, and has drivers for webcam and sound built as modules. It uses an initramfs generated through `dracut`.

The kernel version this config is built for is `5.15.52`, so if you have a newer kernel source, please upgrade the .config as described [here](https://wiki.gentoo.org/wiki/Kernel/Upgrade) before compiling the kernel.

## Adding steam overlay
Please copy over the package.\* files over to your portage directory, then run the following:

```bash
layman -a steam-overlay
emerge -av --autounmask-write games-util/steam-launcher games-util/steam-meta
```
