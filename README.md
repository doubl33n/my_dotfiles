# My Dotfiles

Various dotfiles and other configuration files for Linux distros (mainly Arch & Gentoo). 

## Description
In this repository, you will be able to find configuration files for the Arch and Gentoo package managers, as well as my `bspwm` rice and other config files for other programs. Folders will come with their own description if necessary. Enjoy!

![Screenshot](assets/screenshot.png)

## Info

- **Window Manager:** `bspwm`
- **Hotkey Manager:** `sxhkd`
- **Wallpaper Setter:** `feh`
- **Wallpaper:** From [this](https://www.reddit.com/r/wallpaper/comments/kl2l9i/minimalistic_backround_red_3840x2160/) reddit post by u/Top\_Ad\_666 
- **Terminal:** `kitty`/`st`
- **Shell:** `zsh`
- **Compositor:** `picom` 
- **Font:** MesloLGS Nerd Font
- **Editor:** `vim`/`nvim` 
- **App launcher:** `rofi`
- **GTK-Theme:** Abrus Dark

## Improvements
I will periodically update this repository to reflect my newest changes to my setup. If you have recommendations for improvement, feel free to open an issue and I'll look into it!
